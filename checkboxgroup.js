import { LightningElement, track, api } from "lwc";

export default class Checkboxgroup extends LightningElement {

    @track obj = {
      raceValues: []
    };
    @track raceValuesArray = [];
    
    raceOptions = [{
      label: "Asian, including South Asian",
      value: "Asian, including South Asian"
    },
    {
      label: "Native Hawaiian or Pacific Islander",
      value: "Native Hawaiian or Pacific Islander"
    },
    {
      label: "Black, including African American or Afro-Caribbean",
      value: "Black, including African American or Afro-Caribbean"
    },
    {
      label: "Native American or Alaskan Native",
      value: "Native American or Alaskan Native"
    },
    {
      label: "Unknown",
      value: "Unknown"
    },
    {
      label: "White",
      value: "White"
    },
    ];
    
    handleInputChange(event) {
    this.obj[event.target.name] = event.target.value;
    
    if (event.target.name == "raceValues") {
      if (event.target.value.includes("Unknown")) {
        if(!this.raceValuesArray.includes("Unknown")){
          this.obj[event.target.name] = ["Unknown"];
        }
        else{
          this.obj[event.target.name] = event.target.value.filter(e => e !== 'Unknown');
        }
      } 
      this.raceValuesArray = event.target.value;
    }
}